package com.hurnyakyaroslav;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class PizzasServlet extends HttpServlet {
    private Logger logger = LogManager.getLogger(PizzasServlet.class);
    private PizzaCompany pizzaCompany = new PizzaCompany();


    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");

        //Навігація
        out.println("<p> <a href='hello'>To HELLO</a> </p>");

        //Поточний ліст
        ArrayList<Pizza> pizzas = (ArrayList) pizzaCompany.getList();
        out.println("<H2>List of pizzas </H2>");
        for (Pizza pizza : pizzas) {
            out.println("<p>" + "ID: " + pizza.getId()
                    + " name = " + pizza.getName()
                    + "</p>");
        }

        //Форми
        out.println("<form action='pizzas' method='POST'>" +
                " Name: <input type='text' name='pizza_name'>\n" +
                " <button type='submit'>Add pizza</button>" +
                "</form>");

        out.println("<form action='pizzas' method='PUT'>" +
                " New name: <input type='text' name='pizza_name'>\n" +
                " ID: <input type='text' name='pizza_id'>\n" +
                " <button type='submit'>Update pizza</button>" +
                "</form>");

        out.println("<form action='pizzas' method='DELETE'>" +
                "DELETE" +
                "<p> Pizza id: <input type='text' name='pizza_id'>" +
                " <button type='submit'>Delete</button>" +
                "</form>");


        out.println("</body></html>");
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("pizza_name");
        logger.info("Adding element with name: " + name);
        pizzaCompany.putPizza(new Pizza(name));
        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Deleting element with id: " + Integer.valueOf(req.getParameter("pizza_id")));
        pizzaCompany.removePizza(Integer.valueOf(req.getParameter("pizza_id")));
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.valueOf(req.getParameter("pizza_id"));
        String name = req.getParameter("pizza_name");
        logger.info("Updating element with next name: " + name);
        pizzaCompany.updatePizza(name, id);
       // doGet(req, resp);
    }

    public void destroy() {
        logger.info("Servlet " + this.getServletName() + " has stopped");
    }


}
