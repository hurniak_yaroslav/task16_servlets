package com.hurnyakyaroslav;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebFilter("/pizzas")
public class NameFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
        Optional<String> newPizzaName = Optional.ofNullable(req.getParameter("pizza_name"));
        if (newPizzaName.filter(name -> !name.matches("[A-Za-z]*")).isPresent()) {
            ((HttpServletResponse) resp).sendError(403, "Name is forbidden!");
            return;
        }
        filterChain.doFilter(req, resp);
    }
}
