package com.hurnyakyaroslav;


public class Pizza {

    private static int unique = 0;
    private String name;
    private int id;

    Pizza(String name) {
        this.name = name;
        id = unique;
        unique++;
    }

    Pizza(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
