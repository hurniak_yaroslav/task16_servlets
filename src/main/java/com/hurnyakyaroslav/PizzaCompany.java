package com.hurnyakyaroslav;

import java.util.*;

public class PizzaCompany {

    private Map<Integer, Pizza> pizzas;

    PizzaCompany() {
        pizzas = new HashMap();
        putPizza(new Pizza("Pepperoni"));
        putPizza(new Pizza("Cheese"));
        putPizza(new Pizza("Student`s"));
    }

    public List<Pizza> getList() {
        List<Pizza> info = new ArrayList<>();
        pizzas.forEach((index, pizza) -> {
            info.add(new Pizza(pizza.getName(), pizza.getId()));
        });
        return info;
    }

    public void putPizza(Pizza pizza){
        pizzas.put(pizza.getId(), pizza);
    }
    public void updatePizza(String name, int id){
        Pizza pizza = pizzas.get(id);
        pizza.setName(name);
    }

    public void removePizza(int id){
        pizzas.remove(id);
    }

}
